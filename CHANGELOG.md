# CHANGELOG

This is an old changelog to which the new information will be appended. The "next entry here" text below allows go-semrel-gitlab to do this.

<!--- next entry here -->

## 3.0.1
2020-12-23

### Fixes

- use whole pipeline also in develop branch (86ccdda02af1c564040b1e7cfb674fea972b7760)
- set an old changelog with a previous version (b63822623205fc78302cc39fc218cc4ea24852bf)

## 2.1.0
2020-12-03

### Features

- modify the pipeline to add show release help (ec9ea90d3579c22f11239860736ff4785feda4f3)

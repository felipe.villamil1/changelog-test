/**
 * @Author Felipe Villamil
 *
 * @Copyright Terabee 2020
 *
 */
#include <iostream>

int main()
{
  std::cout << "Glad you asked..\n\n";
  std::cout << "Terabee Rules!\n";
}
